import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import { BottomNavigation, Text } from 'react-native-paper';

const MusicRoute = () => <Text>Favorites</Text>;

const AlbumsRoute = () => <Text>Recents</Text>;

const RecentsRoute = () => <Text>Contacts</Text>;

const BottomBar = () => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'music', title: 'Favorites', icon: 'star' },
    { key: 'albums', title: 'Recents', icon: 'clock-outline' },
    { key: 'recents', title: 'Contacts', icon: 'account-multiple' },
  ]);

  const renderScene = BottomNavigation.SceneMap({
    music: MusicRoute,
    albums: AlbumsRoute,
    recents: RecentsRoute,
  });

  return (
    <BottomNavigation
        navigationState={{ index, routes }}
        onIndexChange={setIndex}
        renderScene={renderScene}
    />
  );
};

const styles = StyleSheet.create({
    container: {
      paddingVertical: 30,
      flex: 1,
      width: '100%',
      backgroundColor: '#f4f4f4',
    },
  });

export default BottomBar;