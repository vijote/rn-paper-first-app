import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { DefaultTheme, Searchbar, Provider as PaperProvider } from 'react-native-paper';
import BottomBar from './components/BottomBar';

const primary = '#2952C0';

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: primary,
    accent: '#f1c40f',
  },
};

export default function App() {
  const [searchQuery, setSearchQuery] = React.useState('');

  const onChangeSearch = query => setSearchQuery(query);

  return (
    <PaperProvider theme={theme}>
      <View style={styles.container}>
        <View style={styles.searchContainer}>
          <Searchbar
            placeholder="Search contacts & places"
            onChangeText={onChangeSearch}
            value={searchQuery}
          />
        </View>
        <BottomBar/>
      </View>
    </PaperProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 30,
    flex: 1,
    width: '100%',
    backgroundColor: '#f4f4f4',
  },
  searchContainer: {
    padding: 10,
    backgroundColor: primary
  }
});
